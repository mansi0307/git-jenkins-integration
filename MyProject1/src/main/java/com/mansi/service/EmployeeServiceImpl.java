package com.mansi.service;

import com.mansi.dao.EmployeeDao;
import com.mansi.dao.EmployeeDaoImpl;
import com.mansi.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {
	
	EmployeeDao empDao = new EmployeeDaoImpl();

	public boolean create(Employee employee) {
		
		empDao.addEmployee(employee);
		System.out.println(empDao.getAll());
		return true;
	}

	
}
