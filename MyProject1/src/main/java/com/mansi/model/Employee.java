package com.mansi.model;

public class Employee {
	
	int empId;
	String firstName;
	String lastName;
	double salary;
	
	public Employee() {
		super();
	}

	public Employee(int empId, String firstName, String lastName, double salary) {
		
		
		 	if (empId==0)
	        {
	            throw new IllegalArgumentException("Invalid Employee ID");   
	        }
		
		 	if (firstName == null || firstName.length()>20)
	        {
	            throw new IllegalArgumentException("Invalid first name");
	        }
		 	
		 	if (lastName == null || lastName.length()>20)
		    {
		         throw new IllegalArgumentException("Invalid last name");
		    }
		 	if ( salary <= 0 || salary > 999999)
		 	{
		 		throw new IllegalArgumentException("Invalid Salary");
		 	}
		 	
		
		//super();
		this.empId = empId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getFirstName() {
		
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", firstName=" + firstName + ", lastName=" + lastName + ", salary=" + salary
				+ "]";
	}
	
	
	

}
