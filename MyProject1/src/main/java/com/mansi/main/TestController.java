package com.mansi.main;

import com.mansi.model.Employee;
import com.mansi.service.EmployeeService;
import com.mansi.service.EmployeeServiceImpl;

public class TestController {
	

	public static void main(String[] args)
	{
		
			EmployeeService employeeService = new EmployeeServiceImpl();
			
			employeeService.create(new Employee(1, "test1", "test11", 100000.0));
			employeeService.create(new Employee(2, "test2", "test22", 200000.0));
			employeeService.create(new Employee(3, "test3", "test33", 300000.0));
			employeeService.create(new Employee(4, "test4", "test44", 400000.0));
			employeeService.create(new Employee(5, "test5", "test55", 500000.0));
			
			System.out.println("Employees Added");
			
			
		}
	}


