package com.mansi.dao;

import java.util.List;

import com.mansi.model.Employee;

public interface EmployeeDao {
	
	 boolean addEmployee(Employee employee);
	 List<Employee>getAll();


}
